%% Проверка pcg для случайной матрицы
clc, clear, close all

A = rand(5);
X = ones(5, 1);
B = A*X;
x = pcg(A, B);

%% 
clc, clear, close all

A1 = triu(magic(5));
X1 = ones(5, 1);
B1 = A1*X1;
x = pcg(A1, B1);

COND = zeros(1, 96);
I = zeros(1, 96);

for n = 5:5:150
    A2 = rand(n);
    As = A2+A2'+n*eye(n);
    COND(n/5) = cond(As);
    X2 = ones(n, 1);
    B2 = As*X2;
    [x, flag, relres, iter] = pcg(As, B2);
    I(n/5) = iter;
end

figure
plot(COND, I)
xlabel('cond')
ylabel('I')
title('Зависимость количества итераций функции pcg от числа обусловленности матрицы')
legend('I(cond)', 'Location', 'Southeast')
grid minor
