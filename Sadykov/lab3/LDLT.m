function [L, D]= LDLT (A)
n=size(A,1);
L=zeros(n);
D=zeros(n);
dsum=0;
lsum=0;
D(1,1)=A(1,1);

for i=1:n
    L(i,i)=1;
    L(i,1)=A(i,1)/D(1,1);
  
end

for i=2:n
    dsum=0;
   for k=1:i-1
        dsum= dsum+(L(i,k)*L(i,k)*D(k,k));
   end 
   D(i,i)=A(i,i) - dsum;
   
   if i~= n
       for j=i+1:n
           lsum=0;
           for k=1:j-1
                lsum=lsum+(L(i,k)*L(j,k)*D(k,k));
           end
           L(j,i)=(A(j,i) - lsum)/D(i,i);
           
       end
   end
end

end