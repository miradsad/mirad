clc, close all, clear all;
s = 10;
X = ones(s, 1); 
V = [];
Er = [];
A = rand(s);
A = A + A' + 10 * eye(s);
B = A * X;
for k = 0 : 3
    V = [V k];
    M = A;
    p = M(1,1);
    for i = 1 : size(A,1)
        for j = 1 : size(A,1)
            if M(i,j) > p
                p = M(i,j);
                f = i;
                g = j;
            else
                f = 1;
                g = 1;
            end
        end
    end
    if rand < 0.5
        d = 1;
    else 
        d = -1;
    end
    M(f,g) = p * (1 + 0.01 * k * d);
    
    [L, D] = LDLT(M);
    LT = L';
    n=size(A,1);
    y=zeros(n,1);
    w=zeros(n,1);
    X0=zeros(n,1);
    for i = 1:n
        for k = 1:(i-1)
            y(i) = y(i) - L(i,k)*y(k);
        end
        y(i) = (y(i)+B(i))/L(i,i);
    end
    for i = 1:n
        w(i) = y(i)/D(i,i);
    end
    for i = n:-1:1
        for k = n:-1:(i+1)
            X0(i) = X0(i) - LT(i,k)*X0(k);
        end
        X0(i) = (X0(i)+w(i))/LT(i,i);
    end
    c = 100 * norm( X0 - X ) / norm(X);
    Er = [Er c];
end
plot(V, Er)
grid on
title("Зависимость относительной погрешности от возмущения максимального элемента матрицы")
xlabel("Процент возмущения")
ylabel("Относительная погрешность (%)")
