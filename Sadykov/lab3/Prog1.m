clc, close all, clear all
X = ones(10, 1);
%cond = 1;
A = rand(10);
A = A + A';

[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 1; %задаем число обусловленности
M = u*d*v';
c1 = cond(M);
B = M*X; 
tic
[L, D]=LDLT(M);
LT = L';
n=size(A,1);
y=zeros(n,1);
w=zeros(n,1);
X0=zeros(n,1);
for i = 1:n
    for k = 1:(i-1) 
        y(i) = y(i) - L(i,k)*y(k);
    end
    y(i) = (y(i)+B(i))/L(i,i);
end
for i = 1:n
    w(i) = y(i)/D(i,i);
end
for i = n:-1:1
    for k = n:-1:(i+1) 
        X0(i) = X0(i) - LT(i,k)*X0(k);
    end
    X0(i) = (X0(i)+w(i))/LT(i,i);
end
t1 = toc;
h1 = norm(X0 - X);

%cond = 10
A = rand(10);
A = A + A';
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 10; 
M = u*d*v';
c2 = cond(M);
B = M*X;
tic
[L, D]=LDLT(M); 
LT = L';
n=size(A,1);
y=zeros(n,1);
w=zeros(n,1);
X0=zeros(n,1);
for i = 1:n
    for k = 1:(i-1) 
        y(i) = y(i) - L(i,k)*y(k);
    end
    y(i) = (y(i)+B(i))/L(i,i);
end
for i = 1:n
    w(i) = y(i)/D(i,i);
end
for i = n:-1:1
    for k = n:-1:(i+1) 
        X0(i) = X0(i) - LT(i,k)*X0(k);
    end
    X0(i) = (X0(i)+w(i))/LT(i,i);
end
t2 = toc;
h2 = norm(X0 - X);

%cond = 100
A = rand(10);
A = A + A';
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 100; 
M = u*d*v';
c3 = cond(M);
B = M*X;
tic
[L, D]=LDLT(M); 
LT = L';
n=size(A,1);
y=zeros(n,1);
w=zeros(n,1);
X0=zeros(n,1);
for i = 1:n
    for k = 1:(i-1) 
        y(i) = y(i) - L(i,k)*y(k);
    end
    y(i) = (y(i)+B(i))/L(i,i);
end
for i = 1:n
    w(i) = y(i)/D(i,i);
end
for i = n:-1:1
    for k = n:-1:(i+1) 
        X0(i) = X0(i) - LT(i,k)*X0(k);
    end
    X0(i) = (X0(i)+w(i))/LT(i,i);
end
t3 = toc;
h3 = norm(X0 - X);

%cond = 1000
A = rand(10);
A = A + A';
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 1000; 
M = u*d*v';
c4 = cond(M);
B = M*X;
tic
[L, D]=LDLT(M); 
LT = L';
n=size(A,1);
y=zeros(n,1);
w=zeros(n,1);
X0=zeros(n,1);
for i = 1:n
    for k = 1:(i-1) 
        y(i) = y(i) - L(i,k)*y(k);
    end
    y(i) = (y(i)+B(i))/L(i,i);
end
for i = 1:n
    w(i) = y(i)/D(i,i);
end
for i = n:-1:1
    for k = n:-1:(i+1) 
        X0(i) = X0(i) - LT(i,k)*X0(k);
    end
    X0(i) = (X0(i)+w(i))/LT(i,i);
end
t4 = toc;
h4 = norm(X0 - X);

%cond = 100000
A = rand(10);
A = A + A';
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 10000; 
M = u*d*v';
c5 = cond(M);
B = M*X;
tic
[L, D]=LDLT(M); 
LT = L';
n=size(A,1);
y=zeros(n,1);
w=zeros(n,1);
X0=zeros(n,1);
for i = 1:n
    for k = 1:(i-1) 
        y(i) = y(i) - L(i,k)*y(k);
    end
    y(i) = (y(i)+B(i))/L(i,i);
end
for i = 1:n
    w(i) = y(i)/D(i,i);
end
for i = n:-1:1
    for k = n:-1:(i+1) 
        X0(i) = X0(i) - LT(i,k)*X0(k);
    end
    X0(i) = (X0(i)+w(i))/LT(i,i);
end
t5 = toc;
h5 = norm(X0 - X);

%cond = 100000
A = rand(10);
A = A + A';
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 100000; 
M = u*d*v';
c6 = cond(M);
B = M*X;
tic
[L, D]=LDLT(M); 
LT = L';
n=size(A,1);
y=zeros(n,1);
w=zeros(n,1);
X0=zeros(n,1);
for i = 1:n
    for k = 1:(i-1) 
        y(i) = y(i) - L(i,k)*y(k);
    end
    y(i) = (y(i)+B(i))/L(i,i);
end
for i = 1:n
    w(i) = y(i)/D(i,i);
end
for i = n:-1:1
    for k = n:-1:(i+1) 
        X0(i) = X0(i) - LT(i,k)*X0(k);
    end
    X0(i) = (X0(i)+w(i))/LT(i,i);
end
t6 = toc;
h6 = norm(X0 - X);

h = [h1 h2 h3 h4 h5 h6];
c = [c1 c2 c3 c4 c5 c6];
t = [t1 t2 t3 t4 t5 t6];
figure
loglog(c, h)
title("График зависимости количества ошибок от числа обусловленности")
xlabel("Число обусловленности")
ylabel("Прологарифмированная норма отклонения")
grid on

figure
semilogx(c, t)
title("График зависимости времени от числа обусловленности")
xlabel("Число обусловленности")
ylabel("Прологарифмированное время")
grid on



