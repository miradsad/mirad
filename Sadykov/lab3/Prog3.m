clc, close all, clear all;
s = 10;
X = ones(s, 1); 
V = [];
Er = [];
A = rand(s);
A = A + A' + 10 * eye(s);

n=size(A,1);
B = A * X;
for k = 0 : 3
    V = [V k];
    F = B;
    p = F(1);
    for i = 1 : length(B)
        if F(i) > p
            p = F(i);
            f = i;
        else
            f=1;
        end
    end
    if rand < 0.5
        d = 1;
    else 
        d = -1;
    end
    F(f) = p * (1 + 0.01 * k * d);
    [L, D] = LDLT(A);
    LT = L';
    y=zeros(n,1);
    w=zeros(n,1);
    X0=zeros(n,1);
    for j = 1:n
        for t = 1:(j-1) 
            y(j) = y(j) - L(j,t)*y(t);
        end
        y(j) = (y(j)+F(j))/L(j,j);
    end
    for j = 1:n
        w(j) = y(j)/D(j,j);
    end
    for j = n:-1:1
        for t = n:-1:(j+1) 
            X0(j) = X0(j) - LT(j,t)*X0(t);
        end
        X0(j) = (X0(j)+w(j))/LT(j,j);
    end
    X0
    c = 100 * norm( X0 - X ) / norm(X);
    Er = [Er c];
end
plot(V, Er)
grid on
title("Зависимость относительной погрешности от возмущения максимального элемента столбца правой части")
xlabel("Процент возмущения")
ylabel("Относительная погрешность (%)")

