function [y]= sp(xx, yy, x,f1)
    A=[];
    B=[];
    C=[];
    D=[];

    n=3;

    sds = 0;
    if mod(length(xx), 3) == 2
        sds = 1+floor(length(xx)/3);
    else
        sds=floor(length(xx)/3);
    end
        
    for k= 1:1:sds 
        if mod(length(xx),3)==0
            if length(xx)==k+n-1
               AA=[xx(k+n-3).^3 xx(k+n-3).^2 xx(k+n-3) 1;
               xx(k+n-2).^3 xx(k+n-2).^2 xx(k+n-2) 1;
               xx(k+n-1).^3 xx(k+n-1).^2 xx(k+n-1) 1;
               3*xx(k+n-1).^2 2*xx(k+n-1) 1 0;];
               BB=[yy(k+n-3); yy(k+n-2); yy(k+n-1); f1(xx(k+n-1))]; 
            else
               AA=[xx(k+n-3).^3 xx(k+n-3).^2 xx(k+n-3) 1;
               xx(k+n-2).^3 xx(k+n-2).^2 xx(k+n-2) 1;
               xx(k+n-1).^3 xx(k+n-1).^2 xx(k+n-1) 1;
               xx(k+n).^3 xx(k+n).^2 xx(k+n) 1];
               BB=[yy(k+n-3); yy(k+n-2); yy(k+n-1); yy(k+n)];
            end
        end
        if mod(length(xx),3)==2
            if length(xx)==k+n-2
               AA=[xx(k+n-3).^3 xx(k+n-3).^2 xx(k+n-3) 1;
               xx(k+n-2).^3 xx(k+n-2).^2 xx(k+n-2) 1;
               3*xx(k+n-3).^2 2*xx(k+n-3) 1 0;
               3*xx(k+n-2).^2 2*xx(k+n-2) 1 0];
               BB=[yy(k+n-3); yy(k+n-2); f1(xx(k+n-3)); f1(xx(k+n-2))]; 
               
            else
               AA=[xx(k+n-3).^3 xx(k+n-3).^2 xx(k+n-3) 1;
               xx(k+n-2).^3 xx(k+n-2).^2 xx(k+n-2) 1;
               xx(k+n-1).^3 xx(k+n-1).^2 xx(k+n-1) 1;
               xx(k+n).^3 xx(k+n).^2 xx(k+n) 1];
               BB=[yy(k+n-3); yy(k+n-2); yy(k+n-1); yy(k+n)];
            end
        end
        if mod(length(xx),3)==1
        AA=[xx(k+n-3).^3 xx(k+n-3).^2 xx(k+n-3) 1;
            xx(k+n-2).^3 xx(k+n-2).^2 xx(k+n-2) 1;
            xx(k+n-1).^3 xx(k+n-1).^2 xx(k+n-1) 1;
            xx(k+n).^3 xx(k+n).^2 xx(k+n) 1;];
        BB=[yy(k+n-3); yy(k+n-2); yy(k+n-1); yy(k+n)];
        end
       
        Ad=det(AA);
        CC=AA;
        X=[];
        for i = 1:1:4
            AA(:,i)= BB(:,1);
            Y=det(AA);
            AA(:,i)=CC(:,i);
            X(i)=Y/Ad;
        end
        A=[A X(1)];
        B=[B X(2)];
        C=[C X(3)];
        D=[D X(4)];
        n=n+2;
    end
    if x<0
        for  j=1:1:(length(xx))
            if xx(j)>x
                index=j-1;
                break;
            end
        end
    else
        index= find(xx<x,1,'last');
    end
    if mod(index,3)<1
        z=floor(index/3);
    else
        z= 1+floor(index/3);
    end
    for i=1:1:length(xx)
        if x==xx(i)
            y=yy(i);
        else
            y= A(z)*x.^3 + B(z)*x.^2 + C(z)*x + D(z);
        end
    end
end