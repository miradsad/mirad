clc;
clear;

%Splinetool
X = linspace(-3, 3, 8);
Y = @(X) sign(X)*X.^4 - 18*X.^2 + 2;
splinetool(X,Y(X));

