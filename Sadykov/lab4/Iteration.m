clc, clear , close all;
A = rand(10);
A = A + A' + 5 * eye(10);
X = ones(10, 1);
B = A * X;

itslau1(A, B, X, 10^(-15))%График зависимости погрешности от заданной точности
figure
itslau2(A, B, 10^(-15))%График зависимости количества итераций от заданной точности
figure
itslau3(A, B)%Графики зависимости погрешности от количества итераций
function [] = itslau1(A, B, X, eps)
    EPS = [];
    error = [];
    for i = 0 : 15
        [X0,n,sa,as]=zei(A, B,eps,1000);
        error = [error norm(X0 - X)];
        EPS = [EPS eps];
        eps = eps * 10;
    end
    loglog(EPS, error)
    grid on
    title('График зависимости погрешности от заданной точности')
    ylabel('погрешность')
    xlabel('заданная точность')
end
function [] = itslau2(A, B, eps)
    EPS = [];
    Kol = [];
    for i = 0 : 15
        [X0,n,sa,as]=zei(A, B,eps,1000);
        Kol = [Kol n];
        EPS = [EPS eps];
        eps = eps * 10;
    end
    semilogx(EPS, Kol)
    grid on
    title('График зависимости количества итераций от заданной точности')
    ylabel('количество итераций')
    xlabel('заданная точность')
end
function [] = itslau3(A, B)
    [X0,n,Kol,Roots]=zei(A, B,10^(-15),1000);
    plot(Kol,Roots)
    grid on
    title('График зависимости погрешности от количества итераций')
    xlabel('количество итераций')
    ylabel('погрешность')
    figure
    semilogy(Kol,Roots)
    grid on
    title('График зависимости погрешности от количества итераций в логарифмическом масштабе')
    xlabel('количество итераций')
    ylabel('погрешность')
end


function [x1,n,Kol,Roots]= zei (A, B , eps, max)
Kol = [];
n = size( A,1);
Roots = [];
L = zeros(n);
D = zeros(n);
R = zeros(n);
    for i = 1:n
        D(i,i) = A(i,i);
    end
    for i = 2:n
        for j = 1:i-1
            L(i,j) = A(i,j);
            R(j,i) = A(j,i);
        end
    end
 
    x= zeros(n,1);
    for i = 0:max
        n = i+1;
        Kol = [Kol i];
        x1 = -1 * inv(L + D) * R * x + inv(L + D) * B;
        XX = x1 - x;
        P = XX(1);
        for k = 2:length(XX)
            if P < XX(k)
                P = XX(k);
            end
        end
        Roots = [Roots P];
        if P < eps
            break
        end
        x = x1;
    end
end