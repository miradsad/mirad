function [x,m,Kol,Roots]= zei (A, B , eps, max)
Kol = [];
n = size( A,1);
Roots = [];
    x= zeros(n,1);
    for i = 0:max - 1
        m = i+1;
        Kol = [Kol i];
        x1 = x;
        for k = 1 : n
            for j = 1 : n
                if j == k
                    x1(k,1) = x1(k,1); 
                end
                if j > k
                    x1(k,1) = x1(k,1) +(A(k,j) * x(j,1));
                end
                if j < k
                   x1(k,1) = x1(k,1) + (A(k,j) * x1(j,1));
                end
            end
            x1(k,1) = (x1(k,1) - B(k,1)) / (-1 * A(k,k))
        end
        XX = x1 - x;
        P = XX(1,1);
        for k = 2:n
            if P < XX(k,1)
                P = XX(k,1);
            end
        end
        Roots = [Roots P];
        if P < eps
            x = x1;
            break
        end
        x = x1;
    end
end