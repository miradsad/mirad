clc, close all, clear all;
s = 10;
X = ones(s, 1);
V = [];
Er = [];
A = rand(s);
A = A + A' + 10 * eye(s);
B = A * X;
for k = 0 : 3
    V = [V k];
    F = B;
    p = F(1);
    for i = 1 : length(B)
        if F(i) > p
            p = F(i);
            f = i;
        else
            f=1;
        end
    end
    if rand < 0.5
        d = 1;
    else 
        d = -1;
    end
    F(f) = p * (1 + 0.01 * k * d);
    [X0,n]=zei(A, F, 10^(-15),1000);
    c = 100 * norm( X0 - X ) / norm(X);
    Er = [Er c];
end
plot(V, Er)
grid on
title("Зависимость относительной погрешности от возмущения максимального элемента столбца правой части")
xlabel("Процент возмущения")
ylabel("Относительная погрешность (%)")


function [x1,n,Kol,Roots]= zei (A, B , eps, max)
Kol = [];
n = size( A,1);
Roots = [];
L = zeros(n);
D = zeros(n);
R = zeros(n);
    for i = 1:n
        D(i,i) = A(i,i);
    end
    for i = 2:n
        for j = 1:i-1
            L(i,j) = A(i,j);
            R(j,i) = A(j,i);
        end
    end
 
    x= zeros(n,1);
    for i = 0:max
        n = i+1;
        Kol = [Kol i];
        x1 = -1 * inv(L + D) * R * x + inv(L + D) * B;
        XX = x1 - x;
        P = XX(1);
        for k = 2:length(XX)
            if P < XX(k)
                P = XX(k);
            end
        end
        Roots = [Roots P];
        if P < eps
            break
        end
        x = x1;
    end
end