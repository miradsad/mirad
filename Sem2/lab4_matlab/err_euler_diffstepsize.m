f = @(t,y) 5*(y-t^2);
a = 0;
b_values = 0.5:0.5:2;
y0 = 0.08;
h_values = 0.08:-0.01:0.01;

% Инициализируем массивы для хранения значений ошибок
err = zeros(length(b_values),length(h_values));

% Вычисляем точное решение
t_exact = a:0.001:max(b_values);
y_exact = t_exact.^2 + 0.4*t_exact + 0.08;

% Решаем ОДУ с помощью явного метода Эйлера для каждого значения шага и каждого отрезка интегрирования
for i = 1:length(b_values)
    b = b_values(i);
    for j = 1:length(h_values)
        h = h_values(j);

        % Вычисляем число шагов
        N = floor((b-a)/h);

        % Инициализируем массивы для хранения значений t и y
        t = zeros(N+1,1);
        y = zeros(N+1,1);

        % Задаем начальные значения
        t(1) = a;
        y(1) = y0;

        % Выполняем итерации метода Эйлера
        for k = 1:N
            t(k+1) = t(k) + h;
            y(k+1) = y(k) + h * f(t(k),y(k));
        end

        % Вычисляем погрешность решения как максимальную разность между точным и приближенным решениями
        y_interp = interp1(t,y,t_exact);
        err(i,j) = max(abs(y_interp-y_exact));

        % Строим график решения для первого отрезка интегрирования
        if i == 1
            figure(1)
            plot(t,y)
            hold on
        end
    end
end

% Строим график точного решения для первого отрезка интегрирования
figure(1)
plot(t_exact,y_exact,'k')
hold off

h_values_str = arrayfun(@(x) sprintf('%0.2g', x), h_values, 'UniformOutput', false);
legend([h_values_str(:); 'Exact'])
xlabel('t')
ylabel('y')

% Строим графики погрешности решения в зависимости от шага для каждого отрезка интегрирования
figure(2)
for i = 1:length(b_values)
    plot(h_values,err(i,:))
    hold on
end
hold off

legend(cellstr(num2str(b_values', 'b=%0.2g')))
xlabel('Step size')
ylabel('Error')
