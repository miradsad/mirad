f = @(x,y) x^2 + 2*x +(y/(x+2));
a = -1;
b = 1;
y0 = 3/2;

options = odeset('RelTol',1e-4,'AbsTol',1e-6);
[x1,y1] = ode23(f,[a b],y0,options);
[x2,y2] = ode45(f,[a b],y0,options);

% Вычисляем значения шага как разность между соседними элементами массива x
h1 = diff(x1);
h2 = diff(x2);

% Строим графики
figure
plot(x1(1:end-1),h1,'r',x2(1:end-1),h2,'b')
legend('ode23','ode45')
xlabel('x')
ylabel('Step size')