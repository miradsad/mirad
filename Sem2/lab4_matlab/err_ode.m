f = @(x,y) x^2 + 2*x +(y/(x+2));
a = -1;
b = 1;
y0 = 3/2;

% Задаем массив точностей
tolerances = logspace(-3,-10,8);

% Инициализируем массивы для хранения значений ошибок и числа шагов
err23 = zeros(size(tolerances));
steps23 = zeros(size(tolerances));
err45 = zeros(size(tolerances));
steps45 = zeros(size(tolerances));

% Решаем ОДУ с помощью ode23 и ode45 для каждой точности
for i = 1:length(tolerances)
    options = odeset('RelTol',tolerances(i),'AbsTol',tolerances(i));
    [x1,y1] = ode23(f,[a b],y0,options);
    [x2,y2] = ode45(f,[a b],y0,options);
    
    % Вычисляем фактическую ошибку как разность между решениями ode23 и ode45
    y1interp = interp1(x1,y1,x2);
    y2interp = interp1(x2,y2,x1);
    err23(i) = max(abs(y1-y2interp));
    err45(i) = max(abs(y2-y1interp));
    
    % Вычисляем число шагов как длину массива x
    steps23(i) = length(x1);
    steps45(i) = length(x2);
end

% Строим графики
figure
loglog(tolerances,err23,'r',tolerances,err45,'b')
legend('ode23','ode45')
xlabel('Tolerance')
ylabel('Error')

figure
loglog(tolerances,steps23,'r',tolerances,steps45,'b')
legend('ode23','ode45')
xlabel('Tolerance')
ylabel('Number of steps')