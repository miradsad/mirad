n = 10; % Размерность системы
m = 20; % Число матриц
S_values = logspace(2,6,m); % Значения коэффициента жесткости
A = cell(m,1); % Массив для хранения матриц
for i = 1:m
    % Создаем матрицу с заданным коэффициентом жесткости
    S = S_values(i);
    d = [linspace(-S,-1,n-1), -S];
    A{i} = diag(d);
end

x0 = repmat([0;1],n/2,1); % Начальные условия
h_values = 0.1:-0.01:0.01; % Значения шага интегрирования
Tmax = 1/min(abs(d)); % Максимальная длина отрезка интегрирования

% Инициализируем массивы для хранения значений ошибок
err_explicit = zeros(m,length(h_values));
err_implicit = zeros(m,length(h_values));

% Решаем каждую систему с помощью явного и неявного метода Эйлера для каждого значения шага
for i = 1:m
    for j = 1:length(h_values)
        h = h_values(j);
        N = floor(Tmax/h);
        t = (0:N)*h;
        
        % Явный метод Эйлера
        x_explicit = zeros(n,N+1);
        x_explicit(:,1) = x0;
        for k = 1:N
            x_explicit(:,k+1) = x_explicit(:,k) + h * A{i} * x_explicit(:,k);
        end
        
        % Неявный метод Эйлера
        x_implicit = zeros(n,N+1);
        x_implicit(:,1) = x0;
        for k = 1:N
            x_implicit(:,k+1) = (eye(n) - h * A{i}) \ x_implicit(:,k);
        end
        
        % Вычисляем точное решение в конечной точке отрезка интегрирования
        x_exact = expm(A{i}*Tmax)*x0;
        
        % Вычисляем ошибки решений
        err_explicit(i,j) = norm(x_explicit(:,end)-x_exact);
        err_implicit(i,j) = norm(x_implicit(:,end)-x_exact);
    end
end

% Строим графики ошибок решений в зависимости от коэффициента жесткости для каждого значения шага
figure(1)
loglog(S_values,err_explicit)
xlabel('Stiffness coefficient')
ylabel('Error')
title('Explicit Euler method')

figure(2)
loglog(S_values,err_implicit)
xlabel('Stiffness coefficient')
ylabel('Error')
title('Implicit Euler method')

legend(cellstr(num2str(h_values', 'h=%0.2g')))
