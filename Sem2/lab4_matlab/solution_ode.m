f = @(x,y) x^2 + 2*x +(y/(x+2));
a = -1;
b = 1;
y0 = 3/2;

options = odeset('RelTol',1e-4,'AbsTol',1e-6);
[x1,y1] = ode23(f,[a b],y0,options);
[x2,y2] = ode45(f,[a b],y0,options);

plot(x1,y1,'r',x2,y2,'b')
legend('ode23','ode45')