f = @(t,y) 5*(y-t^2);
a = 0;
b = 0.5;
y0 = 0.08;
h_values = 0.08:-0.01:0.01;

% Инициализируем массивы для хранения значений ошибок
err = zeros(size(h_values));

% Вычисляем точное решение
t_exact = a:0.001:b;
y_exact = t_exact.^2 + 0.4*t_exact + 0.08;

% Решаем ОДУ с помощью явного метода Эйлера для каждого значения шага
for i = 1:length(h_values)
    h = h_values(i);
    
    % Вычисляем число шагов
    N = floor((b-a)/h);

    % Инициализируем массивы для хранения значений t и y
    t = zeros(N+1,1);
    y = zeros(N+1,1);

    % Задаем начальные значения
    t(1) = a;
    y(1) = y0;

    % Выполняем итерации метода Эйлера
    for j = 1:N
        t(j+1) = t(j) + h;
        y(j+1) = y(j) + h * f(t(j),y(j));
    end
    
    % Вычисляем погрешность решения как максимальную разность между точным и приближенным решениями
    y_interp = interp1(t,y,t_exact);
    err(i) = max(abs(y_interp-y_exact));
    
    % Строим график решения
    figure(1)
    plot(t,y)
    hold on
end

% Строим график точного решения
figure(1)
plot(t_exact,y_exact,'k')
hold off
h_values_str = arrayfun(@(x) sprintf('%0.2g', x), h_values, 'UniformOutput', false);
legend([h_values_str, {'Exact'}])
xlabel('t')
ylabel('y')

% Строим график погрешности решения в зависимости от шага
figure(2)
plot(h_values,err,'b')
xlabel('Step size')
ylabel('Error')