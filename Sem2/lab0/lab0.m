n = 5:100:2000; % размер матрицы
xx1 = zeros(size(n)); % массив для ошибки метода LU
xxq = zeros(size(n)); % массив для ошибки метода QR

for i = 1:length(n)
    A = -ones(n(i));
    A = tril(A, -1) + eye(n(i));
    A(:, end) = 1;
    xt = rand(n(i),1);
    b = A*xt;
    
    x = A\b; % решение методом косой чертой
    xx1(i) = norm(x-xt)/norm(xt); % ошибка метода LU
    
    [Q, R] = qr(A);
    z = Q'*b;
    x = R\z; % решение методом QR-разложения
    xxq(i) = norm(x-xt)/norm(xt); % ошибка метода QR
end

figure;
semilogy(n, xx1, 'r', n, xxq, 'b');
legend('LU', 'QR');
xlabel('Размер матрицы');
ylabel('Относительная ошибка');