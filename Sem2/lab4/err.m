clear
f = @(x,y) x^2 + 2*x +(y/(x+2))
a = -1;
b = 1;
y0 = 0;
h1 = 0.01;
h2 = 0.001;

[x1,y1] = kutta_merson(f,a,b,y0,h1);
[x2,y2] = kutta_merson(f,a,b,y0,h2);
n=100;
for i=1:n;
    [x,y] = kutta_merson(f,a,b,y0,h1);
    x_exact = linspace(a,b,n);
    y_exact = 0.5*x_exact + 1 + x_exact.^2 + 0.5*x_exact.^3;
    y_interp = interp1(x,y,x_exact);
    error = abs(y_exact-y_interp);
end
figure(3);
semilogy(x_exact,error);
title('Ошибка на заданном интервале')
xlabel('x')
ylabel('Error')


