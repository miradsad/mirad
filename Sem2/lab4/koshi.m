clear
f = @(x,y) x^2 + 2*x +(y/(x+2))
a = -1;
b = 1;
y0 = 3/2;
h1 = 0.01;
h2 = 0.001;
h3= 0.0001;
h4= 0.00001;
h5= 0.000001;
h6= 0.0000001;
[x1,y1] = kutta_merson(f,a,b,y0,h1);
[x2,y2] = kutta_merson(f,a,b,y0,h2);

figure(1);
plot(x1,y1,'r',x2,y2,'b');
legend('h=0.01','h=0.001');
title('Графики точного и численных решений');

h = [h1 h2 h3 h4 h5 h6];
for i=1:length(h)
    i
    [x,y] = kutta_merson(f,a,b,y0,h(i));
    y_exact = 0.5*x + 1 + x.^2 + 0.5*x.^3;
    error(i) = max(abs(y_exact-y));
end
figure(2);
loglog(h,error,'-o');
xlabel('Step size h');
ylabel('Значение y стремится к y0=3/2');
title('График зависимости фактической точности от заданной точности');