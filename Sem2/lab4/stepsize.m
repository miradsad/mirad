clear
f = @(x,y) x^2 + 2*x +(y/(x+2))
a = -1;
b = 1;
y0 = 0;
h1 = 0.01;
h2 = 0.001;
h3= 0.0001;
h4= 0.00001;
h5= 0.000001;
h = [h1 h2 h3 h4 h5];
figure;
for i=1:length(h)
    [x,y] = kutta_merson(f,a,b,y0,h(i));
    step_size = diff(x);
    plot(x(1:end-1),step_size)
    hold on
end
title('График шагов')
xlabel('x')
ylabel('Step size')
legend(string(h))