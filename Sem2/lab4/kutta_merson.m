function [x,y] = kutta_merson(f,a,b,y0,h)
    x = a:h:b;
    y = zeros(1,length(x));
    y(1) = y0;
    for i=1:length(x)-1
        k1 = h*f(x(i),y(i));
        k2 = h*f(x(i)+h/3,y(i)+k1/3);
        k3 = h*f(x(i)+h/3,y(i)+k1/6+k2/6);
        k4 = h*f(x(i)+h/2,y(i)-k2/3+k3);
        k5 = h*f(x(i)+h,y(i)+k1*0.2+k4*0.3+k3*0.4);
        y(i+1) = y(i)+(k1+4*k4+k5)/6;
    end
end
